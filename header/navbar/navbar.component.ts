import {Component, Inject, OnInit} from '@angular/core';
import { HostListener} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {WINDOW, WINDOW_PROVIDERS} from '../../service/window-service';
import {AuthenticationService} from '../../authentication/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  hideToolbarMenu = true;
  user: any;

  constructor(@Inject(DOCUMENT) private document: Document,
              @Inject(WINDOW) private window,
              private authService: AuthenticationService,
              private router:  Router) {

    this.authService.user.subscribe((userInfo) => {
      this.user = userInfo;
    });
  }

  ngOnInit() {
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    const number = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
    if (number > 100) {
      this.hideToolbarMenu = false;
    } else if (!this.hideToolbarMenu && number < 100) {
      this.hideToolbarMenu = true;
     }
  }

  logoutUser() {
    this.authService.logout();
    //this.router.navigate(['/login']);
  }

}
