import { Component, OnInit } from '@angular/core';
import {
  Response
} from '@angular/http';
import {
  FormBuilder,
  FormGroup,
  AbstractControl,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';

import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  form: FormGroup;
  email: AbstractControl;
  loading: boolean;

  error: string;
  successMessage: string;

  constructor(
    fb: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService) {
    this.loading = false;
    this.form = fb.group({
      'email': ['', Validators.required]
    });
    this.email = this.form.controls['email'];
  }

  ngOnInit() {
  }

  submit(): void {
    // console.log('submit');
    if (this.loading || this.form.invalid) {
      return;
    }
    this.loading = true;
    this.error = null;
    this.successMessage = null;
    const targetEmail = this.email.value.trim();
    this.authenticationService.requestPasswordResetToken(targetEmail)
      .delay(1000)
      .subscribe((user: any) => {
        this.loading = false;
        this.successMessage = `Password reset mail sent to ${targetEmail}`;
      }, (res: Response) => {
        this.loading = false;
        try {
          if (res.status === 400) {
            const body = res.json();
            if (body.fieldErrors && body.fieldErrors.email) {
              const emailErrors: any[] = body.fieldErrors.email;
              const uniqueUsernameErrors: any[] = emailErrors.filter((val: any) => val.code === 'KnownEmail');
              if (uniqueUsernameErrors.length) {
                this.error = `No account matches ${targetEmail}`;
                return;
              }
            }
          }
        } catch (error) {
          console.error(error);
        }
        this.error = res.statusText;
      });
  }

}
