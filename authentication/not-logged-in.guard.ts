import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import {AuthenticationService} from "./authentication.service";

@Injectable()
export class NotLoggedInGuard implements CanActivate {

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authenticationService.fetchUser()
      .map((user => {
        if (user) {
          this.router.navigate(['/']);
          return false;
        }
        return true;
      }))
      .catch((err: any, caught: Observable<any>) => {
        return Observable.of(true);
      });
  }
}
