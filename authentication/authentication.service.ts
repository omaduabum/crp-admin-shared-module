import { Injectable, Inject, EventEmitter, OnInit } from '@angular/core';
import {
    Http,
    Response,
    RequestOptions,
    Headers
} from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, Observer, AsyncSubject, BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {environment} from 'app/../environments/environment';
import {LoginModel} from "../model/login";
import {MyConstants} from "../service/constants";
import {catchError} from "rxjs/operators";

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token'
    })
};

@Injectable()
export class AuthenticationService implements OnInit {

    user: BehaviorSubject<any> = new BehaviorSubject(undefined);
    private _user: Object;
    private ongoingFetch: Observable<any>;
    private initialized: boolean;
    lastProtectedUrl: string;

    constructor(
        private http: Http,
        private httpClient: HttpClient,
        private router: Router) {
        this.user.subscribe((user: any) => {
            // console.log('user set');
            this._user = user;
        });
        this.fetch().subscribe((res => {
            this.initialized = true;
        }), (res => {
            this.initialized = true;
        }));
    }

    ngOnInit() {
    }

    clearStaleSession() {
        this.user.next(null);
        location.href = [environment.crpAdminBaseUrl === '/' ? '' : environment.crpAdminBaseUrl, 'login'].join('/');
    }

    register(data: any): Observable<any> {
        return this.http.post(`${environment.adminApiBaseUrl}/user`, data)
            .map((res: Response) => res.json())
            .map((user: any) => {
                // console.log("authorization: " + res.headers.get('authorization'));
                this.user.next(user);
                return user;
            });
    }

    updateUserDetails(data: any): Observable<any> {
        return this.httpClient.post(`${environment.adminApiBaseUrl}/user-details`, data)
            .map((user: any) => {
                this.user.next(user);
                return user;
            });
    }

    changePassword(password: string): Observable<any> {
        return this.httpClient.post(`${environment.adminApiBaseUrl}/change-password`, { password }, { observe: 'response' });
    }

    login(username: String, password: string): Observable<any> {
        // const options = new RequestOptions();
        // options.withCredentials = true;
        // options.headers.set('Content-Type', 'application/json');
        return this.http.post(`${environment.adminApiBaseUrl}/login`, { username, password })
            .map((res: Response) => res.json())
            .map((user: any) => {
                // console.log("authorization: " + res.headers.get('authorization'));
                this.user.next(user);
                return user;
            });
    }

    logInUser(username: String, password: String): Observable<any> {
        return this.http.post(`${environment.adminApiBaseUrl}/login`, {username, password}).map((res: Response) => res.json())
            .map((user: any) => {
                console.log("user");
                console.log(user);
                this.user.next(user)
            });
    }

    requestMailVerificationToken(): Observable<any> {
        return this.httpClient.post(`${environment.adminApiBaseUrl}/request-verification-mail`, { });
    }

    requestPasswordResetToken(email: string): Observable<any> {
        return this.http.post(`${environment.adminApiBaseUrl}/password-reset-request`, { email });
    }

    resetPassword(data: any): Observable<any> {
        return this.http.post(`${environment.adminApiBaseUrl}/reset-password`, data)
            .map((user: any) => {
                this.user.next(user);
                return this._user;
            });
    }

    logout(): Observable<Response> {
        const subject: AsyncSubject<Response> = new AsyncSubject();
        this.http.post(`${environment.adminApiBaseUrl}/logout`, {})
            .subscribe(subject);
        subject.subscribe((res: any) => {
            this.clearStaleSession();
        });
        return Observable.create((observer: Observer<Response>) => {
            subject.subscribe(observer);
        });
    }

    fetchUser(): Observable<any> {
        if (this.initialized) {
            return Observable.of(this._user);
        }
        return this.fetch();
    }

    private fetch() {
        if (!this.ongoingFetch) {
            console.log('fetching user...', new Date());
            // const options = new RequestOptions();
            // options.withCredentials = true;
            this.ongoingFetch = this.http.get(`${environment.adminApiBaseUrl}/me`)
                .map((res: Response) => res.json());

            this.ongoingFetch.subscribe((user: any) => {
                console.log('user fetched', user);
                this.ongoingFetch = null;
                this.user.next(user);
            }, (err: any) => {
                this.user.next(null);
                console.error(err);
            });
        } else {
            console.log('fetching user ongoing...');
        }
        return this.ongoingFetch;
    }

}
