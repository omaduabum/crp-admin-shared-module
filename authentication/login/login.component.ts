import { Component, OnInit } from '@angular/core';
import {
    Response
} from '@angular/http';
import {
    FormBuilder,
    FormGroup,
    AbstractControl,
    Validators, FormControl
} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { LoginModel } from 'crp-admin-shared-module/model/login';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


    loginForm: FormGroup;
    signingUserIn: boolean = false;
    anErrorOccurred: boolean = false;

    ngOnInit() {
        this.createForm();
    }

    constructor(private fb: FormBuilder,
                private loginModel: LoginModel,
                private authService: AuthenticationService,
                private router: Router) {

    }

    createForm() {
        this.loginForm = this.fb.group ({
            'username': new FormControl(this.loginModel.username, [
                Validators.required
            ]),
            'password': new FormControl(this.loginModel.password, [
                Validators.required
            ]),
        });
    }

    logUserIn(){
        this.anErrorOccurred = false;
        let loginCredentials = new LoginModel();
        loginCredentials.username = this.loginForm.get('username').value;
        loginCredentials.password = this.loginForm.get('password').value;
        if(this.loginForm.status === "VALID") {
            this.signingUserIn = true;
            console.log(loginCredentials);
            this.authService.logInUser(loginCredentials.username, loginCredentials.password).subscribe(
                data => this.showServerResponseForSuccess(data),
                error =>  this.showServerResponseForError(error), // error path
            );
        }
    }

    showServerResponseForSuccess(data: any){
        this.signingUserIn = false;
        this.router.navigate(['/dashboard']);
    }

    showServerResponseForError(error: any){
        this.signingUserIn = false;
        this.anErrorOccurred = true;
    }

    resetError() {
        this.anErrorOccurred = false;
    }

}
