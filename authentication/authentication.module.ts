import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './authentication.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import {NotLoggedInGuard} from "./not-logged-in.guard";
import {LoggedInGuard} from "./logged-in.guard";
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import {MyConstants} from "../service/constants";
import { WINDOW_PROVIDERS } from '../service/window-service';
import { LoginModel } from "../model/login";

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        HttpModule,
        ReactiveFormsModule,
        RouterModule,
        NgxErrorsModule
    ],
    exports: [
        LoginComponent,
        ChangePasswordComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent
    ],
    declarations: [
        LoginComponent,
        ChangePasswordComponent,
        ResetPasswordComponent,
        ForgotPasswordComponent
    ],
    providers: [AuthenticationService,
                LoggedInGuard,
                NotLoggedInGuard,
                MyConstants,
                WINDOW_PROVIDERS,
                LoginModel]
})
export class AuthenticationModule { }
