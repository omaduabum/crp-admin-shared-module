import {Injectable} from "@angular/core";

@Injectable()
export class MyConstants{

  public static _HOSTNAME_URL = "/crp-admin-api/v1";
  public static _LOGIN_ENDPOINT = "/login";


  public static get LOGIN_ENDPOINT(): string {
    return this._LOGIN_ENDPOINT;
  }

  public static set LOGIN_ENDPOINT(value: string) {
    this._LOGIN_ENDPOINT = value;
  }
}
